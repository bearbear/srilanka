卡片世界
2022.02
@林子 社群
@食灯鬼 社群

# 为所欲为🐻🍎进度23/1000
😲What：
- 一种罕见的心理状态。人类孩童时，由于不熟悉社会规则，可能会偶尔出现。
- 但也和大脑发育相关。孩童时期，大脑的特殊部位不够成熟。
- 倘如不加以外部训练，理性功能丧失，就可能出现为所欲为的症状：比如，熊孩子的胡搅蛮缠。随着孩童长大，熟悉社会规律，能够进行抽象的推断，此类状态会随之消失。
- 但少数成年人也可能会出现这种心理状态。尽管十分罕见，还能观察到为所欲为的成年人。
- 比如，不顾后果压榨员工的某些老板，不顾后果压榨子女的老人年，不顾后果贸然投资的股市韭菜们。


🤔Why：
1.为所欲为是一种心理状态，也可以是物质的一种。在合适的条件下，可以被再创造出来。
2. 为所欲为所能实现的欲望的满足感，也许是人类基本心理机制之一，为了让自我感到满足，不惜一切手段，甚至脑子都不要了，也许是人类本性。
3. 为所欲为极易复发。可能和环境相关。人类目前的人文环境，在引导消解欲望，平静自我的方向上，做得不够好。


🙌🏻How：
1.回忆下你感到满足的时刻是怎样的一副画面？
2.尝试下把这个画面换成另外一副画面，体会下转换的难度。


# 自尊🐻🍎进度11/1000

😲What：人类特有产物。人类通过自尊的维护，建立起主观世界，并依照这个独特的世界观进行活动。举一个例子，对于1个模特，美丽得体的外在是其需要花费大量精力维护的东西，因为美丽代表了ta的自我，也是ta获取自我认同，自我尊重的重要途径。




🤔Why：
1.人是脑化动物，能够理解抽象事物。
2.人的心理世界构建里，通过让自我感觉良好的事物来确认自己的存在。



🙌🏻How：
1.观察自己做什么事情时候最自豪？
2.观察别人做什么事情时候最自豪？
3.自己和别人的自豪列表的差异是？


# 辱骂🐻🍎进度23.1/1000

#### What

1. 辱骂指，用侮辱性攻击性负面性语言攻击他人。
2. 辱骂算是一种语言暴力。
3. 比起肢体暴力，语言暴力更加隐蔽，施暴者甚至都不会承认自己正在施加暴力。

#### Why
1. 暴力的好处是，获得快感,施暴者可以以此为乐一辈子。
2. 被施暴者的困境是，如果正面反抗，可能使自己的境遇更加糟糕,被施暴者一辈子忍辱负重反而是一种占优选择。
3. 施暴者享受施暴，受害者享受忍受，双重加强。

#### How
1. 尝试对一个曾经让你感到不快的人，进行语言攻击。体会语言攻击的快感。




# 觉察🐻🍎 进度1/1000

#### What
1. 一种罕见的心理状态。
2. 意味着，自己知道自己在做什么，也就是有自知之明。
3. 比如，自己知道自己在非常生气，知道自己很委屈。而在一般状态下，当自己感到委屈生气，心理状态会是：对方怎么能这样对我？！也就是攻击外界。

#### Why
1. 觉察是心理功能逐渐精细的表现，
2. 粗糙的心理功能:感知外部刺激->回应外部刺激。带有觉察的心理功能是:感知外部刺激->分辨出感知的结果：喜怒哀乐->回应外部刺激。

#### How
1. 当看到美丽的东西，观察下自己那时的状态，用3个词描述下？
（平静的，被牢牢吸引的，心悦诚服的）


# 隔离🐻🍎 进度23.2/1000
1. 一种罕见的心理状态。但，在当前社会压力激增下，越来越高发和普遍。
2. 按照自然的人类行为模式，外界给予刺激，人类的第一反应是，给予各种各样的回应：躲避靠近。但在极端情况下，人类主动选择对外界任何刺激不给予回应：无论发生什么，都不作任何行为。
3. 比如，在长期遭受辱骂后，从刚开始的悲愤恐惧，逐渐进化为，无动于衷毫不在意。隔离对于人类心理状态稳定具有重要保护重要，但其副作用也相当明显：陷入长期的冷漠僵硬，某些情况下这种副作用不可逆转。

#### Why
1. 隔离，是长期遭受心理压力的必然结果。是在冲突环境里，最大化个人效益的最优选择。隔离的坏处不甚明显，而好处太明显了。双重加强下，隔离容易上瘾。

#### How
1. 尝试一天内，对领导同事父母家人朋友，不进行任何回应。体会1次那种，轻松感。那就是隔离的美妙滋味。


# 情感复健🐻🍎 进度23.3/1000
### what
1. 1种极为特殊的人类行为。在上古时代，比如诗经正在发生的年代，人类社会还难以理解过于抽象的规则，因此主要依靠情感生活，真情实感是每日日常。但在文字符号普及的现代社会，人类主要依靠抽象规则生活，从而远离了情感日常。比如，现代人表达自己伤心时，很难直接说：“我心如死灰”，一般表达为：“上天待我不公”或者“我在经历一些挑战。”
2. 情感复健，即为将抽象冷静的陈述或行为，转为更加直接的情感表达：“我喜欢这个。你这样说我很伤心。我现在特别生气。”

#### Why
1. **情感复健的源头是，情感隔离。这是现代社会的基本现实。**
2. 情感复健的用处是，缓解情感隔离的巨大且长期的副作用。
3. 是1项需要长期训练和引导的心理保健活动。
4. 尝试：听歌/旅游/见朋友/吃好吃的，试1次能让自己直接笑/哭出声来的活动。
 

# 存在主义🐻🍎 进度23.4/1000
#### what
1. 存在主义代表哲学家：克尔凯郭尔、陀思妥耶夫斯基、尼采、萨特。
2. 其最著名的提倡来自法国哲学家萨特：“存在先于本质”。
3. 除了人本身之外没有先天决定的道德或灵魂。道德和灵魂都是人在生存中创造的。人没有义务遵守某个道德标准或宗教信仰，却有选择的自由。当评价一个人时，要评价的是他的行为，而不是他的身份，因为人的本质是透过行为被定义的，“人就是他行为的总和”。

#### Why
1. 萨特提出存在主义的背景是第二次世界大战之后。他提出存在主义的标准是，用行为，而不是身份评价，针对的是当年流行于基督教世界里的反犹主义。
2. 即便是第二次世界大战过去了的100年后，用身份评价人类，用明确的等级规范人类行为，依然是主流模式。比如：如果你是学生，你就应该...如果你是公务员，你就应该...如果你是外地人，就是应该...

# 指数🐻🦋进度13.1/1000 
#### What
1. 描述数字的一种方法。N = B(x)。
2. N 是 number,B 是 base，x 是 exponent.
3. 比如，2可以表达为 10(.301),2(1),e(.693)

#### Why
1. 指数是数字的一种表达方式。
2. 它常用的应用渠道是，描述细菌/细胞分裂增长的速度。
3. 它也可以将抽象的数字，分解了几个部分表达。比如：243=3的5次方。在某些情况下，243可以拆分为a部分和b部分。a部分又可以用对数方法计算，谁是底数谁是幂。一个看起来随机的数字，可以被整理为，底数和幂的组合表达。也就是1个大东西，被2种属性的符号代替。有点像，降低随机数的抽象度。比如，3的5次方明显比243容易理解得多。


# log 与 In🐻🦋 进度13.2/1000
#### what
1. 常见的一种对数字的变形手法。
2. 把一堆看起来不规则的数字，用log或in的算法表达，会出现简单的结果。
3. 比如：log1=0,log10=1,log100=2,log1000=3.通常，当观察这4个点1，10，100，1000会觉得很离散，没什么趋势。但是用对数表达时的4个点，就会发现线性关系：0，1，2，3.   

#### why
1. 自然现象里，观察数值时，如果不用算法处理，会发现获取的数值杂乱无章，难以得到1个比较简单的规律。
2. 尤其在溶液浓度逐渐变化，或者细胞增殖时，杂乱数据让人类大脑不好理解。
3. 当用特定算法处理后，杂乱数据里会显现出奇异的规律：比如，当把一把粗盐撒入水中，水中盐分的平均浓度和时间的关系，其实接近于某个神奇的线性。那掌握了这条线性关系，就可以预估，当放入这么多盐分+这么多时间，2个变量后，会得到1个比较准确的结果。我们可以把这个结果算出来。


# e 🐻🦋 进度13.3/1000
#### what
1. 自然对数的底，是3大经典底数之一。
2. e是无理数，数值介于2和3之间，但没有确切的数值。
3. e作为底数，在人口增长和利息计算领域里，十分好用。
#### why
1. 复利的基本计算公式为：A(t)=Pe{rt}



# 集中注意力 🐻🦋 进度37/1000
#### what
1. 集中注意力是1种行为模式，而且是1种在网络社会里少见的行为模式。
2. 它的特点与一心多用的mutil task完全相反：它需要人类在更长时间内保持对1件事情的关注。
3. 它目前的现状是：电子邮件/电视等碎片类似物，正在很有效率摧毁集中注意力的行为。和情感隔离类似，需要复健的动作来恢复。

#### why
1. 人类的大脑的特性，使得这个行为的维持，需要消耗意志力。
2. 可以使用特定的策略（创造隔离的任务，持续排除干扰等），复健这一行为。
3. 我自己意向入职工作第一个挑战，其实也在锻炼注意力的水平：我需要连续4小时在做同一个主题的事。



# 漂浮注意力 🐻🦋 进度37.1/1000
#### what
1. 同一个主题下存在着功能完全不同的任务。而经验越丰富，越能感知到不同任何的区分度如此之大。比如，写作时第一次写稿，凭借的是抒发。这个过程里，不需要字斟句酌，考量逻辑。但是在结稿后，开始审稿改稿，这个时期内，字斟句酌，考量逻辑才是十分必要的。并且往往，改稿的阶段会比写稿痛苦n倍。
2. 对不同功能的任务理解得越深刻，越明白各种任务需要的不同类型的注意力。而我们往往忽略某些重要的任务，把它们理解为，一个顺手就能做完的事。比如，修改提纲，应该成为独立的单独的需要精心准备的任务，也就是专门反复修订提纲。但是大部分人只是把已经写成的东西，归纳为提纲，顺带填上去。
#### why?
1. 在修改提纲这个功能涉及到的，除了“集中注意力”，还有非常不同的一种注意力“漂浮注意力”。甚至据说，能够区分杰出科学家和其他科学家的标准是：能够在2种注意力之间切换：高度集中对某个话题的审视，灵活的寻找能够关联上的有趣问题。


# 蔡格尼克效应 🐻🦋 进度37.2/1000
#### what？
1. 未完成的任务会一直占用短时记忆。
2. 我们的注意力会被这堆未完成的任务干扰。
3. 我们的注意力并不能基于事情的重要性来排序执行任务的先后。
4. 把想法写下来，是最快将它们从短时记忆里清除的办法。

#### how？
1. 刻意在大脑内保存未解决的问题。
2. 然后去做和这个问题不相关的事情。
3. 在打扫/散步/洗澡时候，并不需要使用全神贯注注意力的时候，让大脑回荡起这个问题。
4. 有些问题的答案会有灵光乍现般的解答。


# 自我消耗 🐻🦋 【37.3/1000】
#### what？
1. 任何一种行为，都会产生自我消耗。分解开来，比如：1控制环境 2控制自我 3做出决策 4完成行为。
2. 一旦意志力被消耗殆尽，就难以在短时间内恢复。
再度开采意志力是需要时间的。在意志力匮乏时，人们的智商理性是不在线的。
3. 比如，用标签/经验/偏见筛选候选者，其实是减少自我消耗的行为。因为要完成一次好的筛选，耗费的时间精力和注意力资源比按照简单规则筛选，大得多。

#### how？ **注意力诡计**
1. 与其一开始就要完成并不吸引我们的工作
2. 不如先选出工作里吸引我们的一部分
3. 先做喜欢的部分做起
4. 在工作开始初期尽量降低，对意志力的消耗。


# template🐻🍎 进度13.1/1000

#### What
#### Why
#### How