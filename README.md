# Hello World!
- **Srilanka** is a small island country near Indian Ocean.
- It appears on one of the stamps I bought for a wechatclub as a gift. 
- Srilanka是一片靠近印度洋的小岛国。
- 它出现在一张1970年的邮票上，这张邮票是我参加一个微信读书群，作为礼物购买。

![images (1)](https://user-images.githubusercontent.com/19412465/124720236-83f68300-df3a-11eb-99b7-877fae4067b7.jpeg)

- 网址 https://bearbear.gitlab.io/srilanka/

> 2021.07.07 benbear
