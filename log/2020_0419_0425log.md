# S40W158


> 2020.04.19-04.25


# DUW 日账

```
0425 Calorie Detect Table
0424 6km jogging, 30mins hiit, calorie detect
0423 6km jogging,dVoiceLetter1
0422 6km jogging
0421 6km jogging, 30mins hiit,dLetter1
0420 6km jogging, dLetter1
0419 30mins hiit, dLetter0
```


# 精读
- detect fat and calories
- [Let’s be Real: What’s Really Happening in the Home During COVID | Education World](https://www.educationworld.com/teachers/let%E2%80%99s-be-real-what%E2%80%99s-really-happening-home-during-covid)
- [Rosetta@home](http://boinc.bakerlab.org/rosetta/)
- [21 security tips for working from home during COVID-19](https://protonmail.com/blog/work-from-home-security-guide/?utm_campaign=ww-en-2c-mail-coms_email-trust&utm_source=proton_users&utm_medium=link&utm_content=covid-19_response_free&utm_term=plus_free_4_blog_1)
- [How to protect your data as coronavirus scams skyrocket - ProtonMail Blog](https://protonmail.com/blog/coronavirus-email-scams/?utm_campaign=ww-en-2c-mail-coms_email-trust&utm_source=proton_users&utm_medium=link&utm_content=covid-19_response_free&utm_term=plus_free_4_blog_3)
- [Data – the new oil, or potential for a toxic oil spill? | Cybersecurity & Technology News | Secure Futures | Kaspersky](https://www.kaspersky.com/blog/secure-futures-magazine/data-new-toxic-waste/34184/)
- [Illustrated Guide to Python 3 by Matt Harrison PDF Download - EBooksCart](https://ebookscart.com/illustrated-guide-to-python-3-by-matt-harrison-pdf-download/)
- [Bandcamp Daily Staff | Bandcamp Daily](https://daily.bandcamp.com/contributors/bandcamp-daily-staff)


# 略读
- [The Nutrients in Rice Vs. Potatoes | Healthy Eating | SF Gate](https://healthyeating.sfgate.com/nutrients-rice-vs-potatoes-2871.html)
- [TuneGym](http://tunegym.com/)
- [Nisi Shawl Home Page](http://nisishawl.com/)
- [The Moth | The Art and Craft of Storytelling](https://themoth.org/story-library/podcast)



# 疑问
- home quarantine risk
- data risk

# 怼印象

- @老刘 喜迎现金流;
- @bruce 背单词配置私人工具,积累的单词向何处发力呢?
- @junyu 要求客服半小时反馈电话,总算打通;算法中领悟生活智慧;
- @OMlala 远程工作新习惯,怼圈 issue 强记录
- @shankai 开启英语逆向学习法;并非纯英语小白;半年学完新概念 1-4;
- @班长 美语口语课程值得期待;
- @宁仔 挑大梁创品牌,敢想敢做勤记录
